package money

import "fmt"

type Money struct {
	amount int64
}

func New(dollars, cents int64) *Money {
	return &Money{amount: (dollars * 100) + cents}
}

func FromAmount(amount int64) *Money {
	return &Money{amount: amount}
}

func (m *Money) Percentage(percent float64) *Money {
	intPercent := int64(percent * 100)
	newAmount := m.amount * intPercent
	return FromAmount(newAmount / 10000)
}

func (m *Money) Equals(other *Money) bool {
	return m.amount == other.amount
}

func (m *Money) String() string {
	dollars := m.amount / 100
	cents := m.amount % 100
	return fmt.Sprintf("$%d.%02d", dollars, cents)
}
