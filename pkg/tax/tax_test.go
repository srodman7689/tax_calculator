package tax_test

import (
	"testing"

	"gitlab.com/srodman7689/tax_calculator/pkg/money"
	"gitlab.com/srodman7689/tax_calculator/pkg/tax"
)

func TestSelfEmploymentTax(t *testing.T) {
	hoursForOneHundred := 5.00 + 39.00 + 32.75
	hoursForOneSixty := 16.25
	totalPay := ((hoursForOneHundred * 100) + (hoursForOneSixty * 160)) * 100
	dollars := int64(totalPay) / 100
	cents := int64(totalPay) % 100
	total := money.New(dollars, cents)
	expected := money.New(1572, 7)
	actual := tax.SelfEmployment(total)
	if !expected.Equals(actual) {
		t.Errorf("want: %s, got: %s", expected, actual)
	}
}
