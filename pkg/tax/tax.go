package tax

import (
	"gitlab.com/srodman7689/tax_calculator/pkg/money"
)

const SelfEmploymentTaxPercentage float64 = 15.3

func SelfEmployment(total *money.Money) *money.Money {
	return total.Percentage(SelfEmploymentTaxPercentage)
}
